## Team Name - The Bug Truffles

### Project Overview
----------------------------------
The problem statement was to create an intelligent chatbot which could aggregate and display the user's order status in as precise and accurate manner as possible.

### Solution Description
----------------------------------

#### Architecture Diagram

[Data Flow Diagram](https://drive.google.com/open?id=1VM18RkOoBkI5fCX5G1LGAMr-zq_z3l8h)

#### Technical Description

The chatbot is made on Dialogflow and is integrated with the Firebase database.

### Team Members
----------------------------------
**Ankita Khandelwal** (ak844@snu.edu.in) - worked on Dialogflow;
**Harshit Khandelwal** (hk627@snu.edu.in) - worked on Dialogflow;
**Arnav Sinha** (as872@snu.edu.in) - worked on database integration and query analysis;
**Pranav Jain** (pj386@snu.edu.in) - worked on database integration and query analysis

